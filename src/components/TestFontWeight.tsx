export interface FontWeightTestProps
    extends React.ComponentPropsWithoutRef<'div'> {
    text: string
    font: string
}

export function TestFontWeight({ text, font, ...props }: FontWeightTestProps) {
    return (
        <div {...props}>
            <div style={{ fontFamily: font }} className="font-thin">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-extralight">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-light">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-regular">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-medium">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-semibold">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-bold">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-extrabold">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="font-black">
                {text}
            </div>
        </div>
    )
}

export function TestFontWeightItalic({
    text,
    font,
    ...props
}: FontWeightTestProps) {
    return (
        <div {...props}>
            <div style={{ fontFamily: font }} className="italic font-thin">
                {text}
            </div>
            <div
                style={{ fontFamily: font }}
                className="italic font-extralight"
            >
                {text}
            </div>
            <div style={{ fontFamily: font }} className="italic font-light">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="italic font-regular">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="italic font-medium">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="italic font-semibold">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="italic font-bold">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="italic font-extrabold">
                {text}
            </div>
            <div style={{ fontFamily: font }} className="italic font-black">
                {text}
            </div>
        </div>
    )
}
